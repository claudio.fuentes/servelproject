import React, { Component } from "react"
import DataTable from "react-data-table-component"
import axios from "axios"

export default class Candidatos extends Component {
    constructor() {
        super()
        this.state = {
            rut: '',
            dataCandidatos: [],
            dataPactos: [],
            buscador:''
        }
    }

    componentDidMount() {
    }

    handleChange = (e) => {
        const { name, value } = e.target
        this.setState({ [name]: value })
    }
    handleClick = () => {
        console.log(this.state.rut)
        axios.post('https://api.servel.cl/utils/candidatos', { cedula: this.state.rut })
            .then(resp => {
                const { datos } = resp.data
                this.setState({ dataCandidatos: datos, rut: '',buscador:'' })

            })
            .then(() => {
                 if (this.state.dataCandidatos.length > 0) {
                    this.ArregloDePactos()
                 } 
                })
            .catch(err => alert(err))
    }

    ArregloDePactos = async () => {
        let pactos = []
        await this.state.dataCandidatos.map(candidato => {
            if (pactos.filter(pacto => pacto.pacto === candidato.nombre_pacto).length === 0) {
                pactos.push({ 'pacto': candidato.nombre_pacto, 'total': 0, 'alcaldes': 0, 'gobernadores': 0, 'constituyentes': 0, 'concejales': 0 })
            }
            const index = pactos.findIndex(pacto => pacto.pacto === candidato.nombre_pacto)
            switch (candidato.tipo_eleccion) {
                case 'ALCALDES':
                    pactos[index].alcaldes++
                    break
                case 'CONVENCIONALES CONSTITUYENTES':
                    pactos[index].constituyentes++
                    break
                case 'GOBERNADOR REGIONAL':
                    pactos[index].gobernadores++
                    break;
                case 'CONCEJALES':
                    pactos[index].concejales++
                    break
                default:
                    break;
            }
            pactos[index].total++
        })
        this.setState({ dataPactos: pactos })
    }


    render() {
        const CandidatosColumns =
            [
                { name: "nombre", selector: "nombre_completo", sortable: true },
                { name: "candidato a", selector: 'tipo_eleccion', sortable: true },
                { name: "partido", selector: 'detalle_partido', sortable: true },
                { name: "pacto", selector: 'nombre_pacto', sortable: true, hide:'md'},
                { name: "número", selector: 'voto' }
            ]
        const PactosColumns =
            [
                { name: "nombre", selector: "pacto", sortable: true, minWidth: "30px" },
                { name: "alcaldes", selector: "alcaldes", sortable: true, maxWidth: "10px" },
                { name: "gobernadores", selector: "gobernadores", sortable: true, maxWidth: "10px" },
                { name: "constituyentes", selector: "constituyentes", sortable: true, maxWidth: "10px" },
                { name: "concejales", selector: "concejales", sortable: true, maxWidth: "10px" },
                { name: "totales", selector: "total", sortable: true, maxWidth: "10px" }

            ]
        const showDataCandidatos = this.state.dataCandidatos.filter(candidato => candidato.nombre_completo.search(this.state.buscador.toUpperCase()) > -1)
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-lg-6 offset-lg-2'>
                        <div className='input-group'>
                            <span className='input-group-text'>Rut</span>
                            <input className='form-control' type='number' name='rut' id='txtRut' placeholder='ingrese su rut sin puntos ni dígito verificador' aria-label="Recipient's username" aria-describedby="btnVer" onChange={this.handleChange} value={this.state.rut} />
                            <button className='btn btn-outline-primary' id='btnVer' onClick={this.handleClick}>ver</button>
                        </div>
                    </div>

                </div>
                <div className='row m-5'>
                    <div className='col-lg-4 col-md-6'>
                        <span><label>Buscar</label></span>
                        <input type='text' name='buscador' value={this.state.buscador} placeholder='ingrese un nombre a buscar' style={{width:'100%'}} onChange={this.handleChange}/>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-lg-12'>
                        <DataTable
                            responsive={true}
                            pagination={true}
                            title='Candidatos'
                            columns={CandidatosColumns}
                            data={showDataCandidatos}
                        />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-lg-12'>
                        <DataTable
                            title="Pactos"
                            pagination={true}
                            columns={PactosColumns}
                            data={this.state.dataPactos}
                        />
                    </div>

                </div>
            </div>
        )
    }
}